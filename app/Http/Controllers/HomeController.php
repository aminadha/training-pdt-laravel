<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $users = \DB::table('users')->get();

        $users = \App\User::all();

        return view('home', ['orang' => $users]);
    }
}
