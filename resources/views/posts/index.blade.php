@extends('layouts.master')

@section('content')
	@foreach($posts as $post)
	<div class="blog-post">
		<a href="{{ route('posts.show', $post->id) }}">
	  		<h2 class="blog-post-title">{{ $post->title }}</h2>
	  	</a>
		<p class="blog-post-meta">
			{{ $post->created_at->format('j, M Y H:i A') }} by 
			<a href="#">
				{{ $post->user->name }}
			</a>|
			<a href="{{ route('posts.edit', $post->id) }}">Edit</a>
			
		</p>

		<p>{{ str_limit($post->body, 100, '...') }}</p>
	  	
		<form method="POST" action="{{ route('posts.destroy', $post->id) }}">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="DELETE">
			<button 
				class="btn btn-danger btn-sm" 
				type="submit" 
				onclick="return confirm('Are you sure?')"
			>Delete</button>
		</form>
	</div>
	<!-- /.blog-post -->
	@endforeach

	<nav class="blog-pagination">
	  <a class="btn btn-outline-primary" href="#">Older</a>
	  <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
	</nav>
@endsection

