@extends('layouts.master')

@section('content')
	<form action="{{ route('posts.update', $post->id) }}" method="POST">
		{{ csrf_field() }}
	  <div class="form-group">
	    <label for="title">Title</label>
	    <input 
	    	type="text"
	    	name="title" 
	    	class="form-control" 
	    	id="title" 
	    	placeholder="Enter Title"
	    	value="{{ $post->title }}"
	    >
	  </div>

	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea name="body" class="form-control" id="body" rows="5">{{ $post->body }}</textarea>
	  </div>

	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
@endsection