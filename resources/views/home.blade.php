@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date Created</th>
                        <th>Last Updated</th>
                    </tr>
                    
                    @php $i = 1; @endphp
                    @foreach($orang as $user) 
                    
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at->format('d-m-Y') }}</td>
                        <td>{{ $user->updated_at->format('d-m-Y') }}</td>
                        <td>
                            <a href="#" class="btn btn-default">
                                <i class="glyphicon glyphicon-music"></i>
                            </a>
                        </td>
                    </tr>
                     
                       @php $i++; @endphp
                    @endforeach
                    
                </table> 
            </div>
        </div>
    </div>
</div>
@endsection
