<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')
	->name('home');

// Posts
Route::get('/posts', 'PostsController@index')
	->name('posts.index');

Route::get('/posts/create', 'PostsController@create')
	->name('posts.create');

Route::post('/posts/create', 'PostsController@store')
	->name('posts.store');

Route::get('/posts/{post}', 'PostsController@show')
	->name('posts.show');

Route::get('/posts/{post}/edit', 'PostsController@edit')
	->name('posts.edit');

Route::post('/posts/{post}/edit', 'PostsController@update')
	->name('posts.update');

Route::delete('/posts/{post}', 'PostsController@destroy')
	->name('posts.destroy');









// Route::get('/test', function () {
// 	$users = DB::table('users')->get();
// 	// select * from users
// 	// dump($users);

// 	foreach ($users as $user) {
// 		echo $user->name;
// 		echo '<br>';
		
// 		echo $user->email;
// 		echo '<br>';

// 		echo $user->created_at;
// 		echo '<br><br>';

// 	}

// });
